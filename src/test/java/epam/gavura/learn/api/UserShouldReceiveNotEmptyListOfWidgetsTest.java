package epam.gavura.learn.api;

import epam.gavura.learn.clients.api.DashboardClient;
import epam.gavura.learn.models.User;
import epam.gavura.learn.models.dashboard.Dashboard;
import epam.gavura.learn.properties.PropertiesClient;
import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.Test;

import static epam.gavura.learn.constants.Groups.API_SMOKE;
import static org.apache.http.HttpStatus.SC_OK;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class UserShouldReceiveNotEmptyListOfWidgetsTest {
    @Test(groups = API_SMOKE)
    void userShouldReceiveNotEmptyListOfWidgetsTest() {
        User user = PropertiesClient.getInstance().getUser();
        var content = new DashboardClient()
            .getAllDashboards()
            .checkStatusCode(SC_OK)
            .getBodyAsObject(Dashboard.class)
            .getContent()
            .stream()
            .findFirst()
            .orElseThrow(() -> new IllegalArgumentException("No context is present"));

        assertThat(content.getOwner()).isEqualTo(user.getUserName());
        assertThat(content.getWidgets()).isNotEmpty();
    }
}
