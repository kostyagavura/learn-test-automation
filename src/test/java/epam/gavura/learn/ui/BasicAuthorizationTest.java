package epam.gavura.learn.ui;

import com.codeborne.selenide.Condition;
import epam.gavura.learn.models.User;
import epam.gavura.learn.pages.login.ReportPortalLoginPage;
import epam.gavura.learn.properties.PropertiesClient;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Iterator;
import java.util.List;

import static epam.gavura.learn.constants.Groups.UI_SMOKE;

@Slf4j
public class BasicAuthorizationTest {
    @Test(groups = UI_SMOKE, dataProvider = "basicAuthorizationTestDataProvider")
    public void basicAuthorizationTest(User user, String message) {
        new ReportPortalLoginPage()
            .open()
            .tryToLoginInWithCredentials(user)
            .getNotificationMessage()
            .shouldHave(Condition.text(message));
    }

    @DataProvider(parallel = true)
    public Iterator<Object[]> basicAuthorizationTestDataProvider() {
        User validUser = PropertiesClient.getInstance().getUser();
        User wrongUserNameUser = validUser.toBuilder().userName(RandomStringUtils.randomAlphabetic(10)).build();
        User wrongPasswordUser = validUser.toBuilder().password(RandomStringUtils.randomAlphabetic(10)).build();
        String successMessage = "Signed in successfully";
        String errorMessage = "An error occurred while connecting to server: You do not have enough permissions. Bad credentials";

        return List.of(
                new Object[]{validUser, successMessage},
                new Object[]{wrongUserNameUser, errorMessage},
                new Object[]{wrongPasswordUser, errorMessage})
            .iterator();
    }
}
