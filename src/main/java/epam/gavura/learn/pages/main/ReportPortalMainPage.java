package epam.gavura.learn.pages.main;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import epam.gavura.learn.pages.base.BasePage;

import java.time.Duration;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class ReportPortalMainPage extends BasePage {
    public SelenideElement getNotificationMessage() {
        return $(byXpath("//div[@class = 'notification-transition-enter-done']"))
            .shouldBe(Condition.exist, Duration.ofSeconds(10))
            .lastChild();
    }
}
