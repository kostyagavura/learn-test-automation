package epam.gavura.learn.pages.base;

import com.codeborne.selenide.Configuration;
import epam.gavura.learn.properties.PropertiesClient;


public class BasePage {

    public BasePage() {
        Configuration.baseUrl = PropertiesClient.getInstance().getBaseUrl();
    }
}
