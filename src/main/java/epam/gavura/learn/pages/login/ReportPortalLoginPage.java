package epam.gavura.learn.pages.login;

import com.codeborne.selenide.Selenide;
import epam.gavura.learn.models.User;
import epam.gavura.learn.pages.base.BasePage;
import epam.gavura.learn.pages.main.ReportPortalMainPage;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class ReportPortalLoginPage extends BasePage {
    private static final String LOGIN_PAGE_URI = "/ui/#login";

    public ReportPortalLoginPage open() {
        return Selenide.open(LOGIN_PAGE_URI, ReportPortalLoginPage.class);
    }

    public ReportPortalMainPage tryToLoginInWithCredentials(User user) {
        $(byXpath("//input[@placeholder='Login']")).setValue(user.getUserName());
        $(byXpath("//input[@placeholder='Password']")).setValue(user.getPassword());
        $(byXpath("//button[@type='submit']")).click();

        return Selenide.page(ReportPortalMainPage.class);
    }
}
