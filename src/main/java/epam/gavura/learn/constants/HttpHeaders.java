package epam.gavura.learn.constants;

import lombok.experimental.UtilityClass;

@UtilityClass
public final class HttpHeaders {

    public static final String BEARER = "Bearer ";
}
