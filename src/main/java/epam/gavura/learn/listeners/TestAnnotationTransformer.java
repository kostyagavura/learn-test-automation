package epam.gavura.learn.listeners;

import epam.gavura.learn.properties.PropertiesClient;
import org.testng.IAnnotationTransformer;
import org.testng.annotations.ITestAnnotation;
import org.testng.annotations.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings("PMD.TestClassWithoutTestCases")
public class TestAnnotationTransformer implements IAnnotationTransformer {

    @Override
    public void transform(ITestAnnotation annotation, Class testClass,
                          Constructor testConstructor, Method testMethod) {

        var annotationTypesList = Arrays.stream(testMethod.getAnnotations())
            .map(Annotation::annotationType)
            .toList();


        if (annotationTypesList.contains(Test.class)) {
            var groups = List.of(testMethod.getAnnotation(Test.class).groups());
            if (!groups.contains(PropertiesClient.getInstance().getGroups())) {
                annotation.setEnabled(false);
            }
        }
    }
}
