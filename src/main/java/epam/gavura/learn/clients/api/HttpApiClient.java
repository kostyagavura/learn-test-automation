package epam.gavura.learn.clients.api;

import epam.gavura.learn.models.User;
import epam.gavura.learn.properties.PropertiesClient;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Collectors;

import static epam.gavura.learn.constants.HttpHeaders.BEARER;
import static epam.gavura.learn.utils.JsonUtils.formattedLogsBody;
import static org.apache.http.HttpHeaders.AUTHORIZATION;
import static org.apache.http.HttpHeaders.CONTENT_TYPE;
import static org.apache.http.entity.ContentType.APPLICATION_JSON;

@SuppressWarnings("PMD.GuardLogStatement")
@Slf4j
public class HttpApiClient {

    private final CloseableHttpClient closeableHttpClient;
    protected HttpRequestBase httpRequestBase;

    public HttpApiClient() {
        closeableHttpClient = HttpClients.createDefault();
    }

    protected HttpApiClient buildGETRequest(String endpoint, User user) {
        httpRequestBase = new HttpGet();
        addDefaultHeaders(endpoint, user);

        return this;
    }

    protected HttpApiClient buildDeleteRequest(String endpoint, User user) {
        httpRequestBase = new HttpDelete();
        addDefaultHeaders(endpoint, user);

        return this;
    }

    @SneakyThrows
    protected HttpApiClient buildPostRequest(String endpoint, User user, Object body) {
        httpRequestBase = new HttpPost();
        addDefaultHeaders(endpoint, user);
        ((HttpPost) httpRequestBase).setEntity(new StringEntity(body.toString()));

        return this;
    }

    @SneakyThrows
    protected HttpApiClient buildPutRequest(String endpoint, User user, Object body) {
        httpRequestBase = new HttpPut();
        addDefaultHeaders(endpoint, user);
        ((HttpPut) httpRequestBase).setEntity(new StringEntity(body.toString()));

        return this;
    }

    @SneakyThrows
    protected ResponseHandler execute() {
        loggingRequest(httpRequestBase);
        ResponseHandler responseHandler = new ResponseHandler(closeableHttpClient.execute(httpRequestBase));
        closeableHttpClient.close();

        return responseHandler;
    }

    protected void addDefaultHeaders(String endpoint, User user) {
        httpRequestBase.setURI(URI.create(PropertiesClient.getInstance().getBaseUrl() + endpoint));
        httpRequestBase.setHeader(new BasicHeader(CONTENT_TYPE, APPLICATION_JSON.getMimeType()));
        httpRequestBase.addHeader(new BasicHeader(AUTHORIZATION, BEARER.concat(user.getUserToken())));
    }


    private void loggingRequest(HttpRequestBase httpRequestBase) {
        log.info("\n{}: {} \n{}\nHEADERS:\n{}",
            httpRequestBase.getMethod(),
            httpRequestBase.getURI(),
            logBodyRequest(httpRequestBase),
            Arrays.stream(httpRequestBase.getAllHeaders())
                .map(header -> String.join("", header.getName() + ": " + header.getValue()))
                .sorted(Comparator.naturalOrder())
                .collect(Collectors.joining("\n")));
    }

    @SneakyThrows
    private String logBodyRequest(HttpRequestBase httpRequestBase) {
        return httpRequestBase instanceof HttpEntityEnclosingRequestBase httpEntity
            ? "BODY:\n".concat(formattedLogsBody(EntityUtils.toString(httpEntity.getEntity(), StandardCharsets.UTF_8.name())))
            : "";
    }
}
