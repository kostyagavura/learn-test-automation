package epam.gavura.learn.clients.api;

import epam.gavura.learn.properties.PropertiesClient;

public class DashboardClient extends ServiceClient {

    private static final String DASHBOARD_ENDPOINT = "/api/v1/%s/dashboard";

    private final String currentProjectName = PropertiesClient.getInstance().getProjectName();

    public DashboardClient() {
        super(PropertiesClient.getInstance().getUser());
    }

    public ResponseHandler getAllDashboards() {
        return super.defaultGet(String.format(DASHBOARD_ENDPOINT, currentProjectName));
    }
}
