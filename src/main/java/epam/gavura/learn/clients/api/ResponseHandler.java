package epam.gavura.learn.clients.api;

import epam.gavura.learn.utils.JsonUtils;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static epam.gavura.learn.utils.JsonUtils.formattedLogsBody;

@SuppressWarnings("PMD.GuardLogStatement")
@Slf4j
public class ResponseHandler {

    private String responseBody = "";
    private static final String HEADER_KEY_VALUE_SEPARATOR = ": ";
    private final List<String> responseHeaders;
    private final int responseCode;

    @SneakyThrows
    public ResponseHandler(HttpResponse httpResponse) {
        if (httpResponse.getEntity() != null) {
            this.responseBody = EntityUtils.toString(httpResponse.getEntity(), StandardCharsets.UTF_8.name());
        }
        this.responseCode = httpResponse.getStatusLine().getStatusCode();
        this.responseHeaders = Arrays.stream(httpResponse.getAllHeaders())
            .map(header -> header.getName() + HEADER_KEY_VALUE_SEPARATOR + header.getValue())
            .sorted(Comparator.naturalOrder())
            .toList();
        loggingResponse();
    }

    public ResponseHandler checkStatusCode(int httpStatus) {
        if (responseCode != httpStatus) {
            throw new IllegalArgumentException("The status code in the response is not as expected");
        }

        return this;
    }

    @SneakyThrows
    public <T> T getBodyAsObject(Class<T> clazz) {
        return JsonUtils.fromJson(responseBody, clazz);
    }

    private void loggingResponse() {
        log.info("\nRESPONSE:\nSTATUS CODE:\n{}\nBODY:\n{}\nHEADERS:\n{}",
            responseCode,
            formattedLogsBody(responseBody),
            String.join("\n", responseHeaders));
    }
}
