package epam.gavura.learn.clients.api;

import epam.gavura.learn.models.User;
import epam.gavura.learn.utils.JsonUtils;

public class ServiceClient {

    private final HttpApiClient httpApiClient;
    private final User user;

    public ServiceClient(User user) {
        this.httpApiClient = new HttpApiClient();
        this.user = user;
    }

    protected ResponseHandler defaultGet(String endpoint) {
        return httpApiClient.buildGETRequest(endpoint, user)
            .execute();
    }

    protected ResponseHandler defaultPut(String endpoint, Object body) {
        return httpApiClient.buildPutRequest(endpoint, user, JsonUtils.toJson(body))
            .execute();
    }

    protected ResponseHandler defaultPost(String endpoint, Object body) {
        return httpApiClient.buildPostRequest(endpoint, user, JsonUtils.toJson(body))
            .execute();
    }

    protected ResponseHandler defaultDelete(String endpoint) {
        return httpApiClient.buildDeleteRequest(endpoint, user)
            .execute();
    }
}
