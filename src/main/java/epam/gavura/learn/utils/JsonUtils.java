package epam.gavura.learn.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

@UtilityClass
public class JsonUtils {
    @SneakyThrows
    public static <T> T fromJson(final String json, final Class<T> classOfT) {
        return new ObjectMapper().readValue(json, classOfT);
    }

    @SneakyThrows
    public static String toJson(final Object src) {
        return new ObjectMapper().writeValueAsString(src);
    }

    public static String formattedLogsBody(String stringText) {
        String body;
        try {
            ObjectMapper mapper = new ObjectMapper();
            Object parsedObject = mapper.readValue(stringText, Object.class);
            body = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(parsedObject);
        } catch (JsonProcessingException exception) {
            body = stringText;
        }

        return body;
    }
}
